<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __invoke(Request $request): RedirectResponse
    {
        return match (Auth::user()->role) {
            'instructor' => redirect()->route('instructor.dashboard'),
            'member'     => redirect()->route('member.dashboard'),
            'admin'      => redirect()->route('admin.dashboard'),
            default      => redirect()->route('login'),
        };
    }
}
